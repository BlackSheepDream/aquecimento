﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

namespace WhiteFoxDream.input
{
    public class Controller : MonoBehaviour
    {
        public List<KeyMap> keyMaps;

        public Vector2 Axis
        {
            get
            {
                return new Vector2(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical"));
            }
        }

        private void Update()
        {
            keyMaps.ForEach(KeyEvent);
        }

        private void KeyEvent(KeyMap keyMap)
        {
            if (Input.GetKeyDown(keyMap.key))
                keyMap.OnPressDown.Invoke();
            else if (Input.GetKeyUp(keyMap.key))
                keyMap.OnPressUp.Invoke();
            else if (Input.GetKey(keyMap.key))
                keyMap.OnPress.Invoke();
        }
    }

    [System.Serializable]
    public class KeyMap
    {
        public KeyCode key;
        public UnityEvent OnPressDown;
        public UnityEvent OnPress;
        public UnityEvent OnPressUp;
    }
}
