﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WhiteFoxDream.Player
{
    [RequireComponent(typeof(PlayerMove))]
    [RequireComponent(typeof(PlayerJump))]
    public class PlayerAnimator : MonoBehaviour
    {
        public Animator animator;

        private PlayerJump playerJump;
        private PlayerMove playerMove;

        private void Awake()
        {
            playerJump = GetComponent<PlayerJump>();
            playerMove = GetComponent<PlayerMove>();
            Validator();
        }

        private void Validator()
        {
            if (animator == null)
            {
                this.enabled = false;
                Debug.LogWarning("PlayerAnimator: Animator is Null");
            }
        }

        public float SetVelX
        {
            set
            {
                this.animator.SetFloat("VelX", Mathf.Clamp01(Mathf.Abs(value)));
            }
        }

        private void LateUpdate()
        {
            animator.SetBool("IsGround", playerJump.IsOnGround);
            var velX = Mathf.Abs(playerMove.SpeedX);
            animator.SetFloat("VelX", (playerMove.IsRunning) ? velX : velX - 0.5f);
        }
    }
}