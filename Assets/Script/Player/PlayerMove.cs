﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using WhiteFoxDream.input;

namespace WhiteFoxDream.Player
{
    public class PlayerMove : MonoBehaviour
    {
        public float speed = 5f;
        public float runningSpeed = 9f;

        [Header("Player Dependency")]
        [SerializeField]
        private Controller controller;

        private bool isRunning;

        public bool IsRunning
        {
            get
            {
                return isRunning;
            }

            set
            {
                isRunning = value;
            }
        }

        private float CurrentSpeed
        {
            get
            {
                return (isRunning) ? runningSpeed : speed;
            }
        }

        public float SpeedX
        {
            get
            {
                return controller.Axis.x;
            }
        }

        private void Awake()
        {
            if(controller == null)
            {
                this.enabled = false;
                throw new System.NullReferenceException("Controller is null");
            }
        }

        private void Update()
        {
            transform.position += transform.right * CurrentSpeed * controller.Axis.x * Time.deltaTime;
        }
    }
}