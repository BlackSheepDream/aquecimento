﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace WhiteFoxDream.Player
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class PlayerJump : MonoBehaviour
    {
        [Tooltip("Força aplicada para dar impulso ao player")]
        public float force = 10f;

        [Header("Setup")]
        public Transform origin;
        [Range(0.1f, 10f)]
        public float maxDistanceFloorToJump = 0.5f;
        public LayerMask layerTrigger;

        private Vector3 OriginPosition
        {
            get
            {
                if (origin == null)
                    return transform.position;

                return origin.position;
            }
        }

        public bool IsOnGround
        {
            get
            {
                return Physics2D.Raycast(OriginPosition, Vector3.down, maxDistanceFloorToJump, layerTrigger).collider != null;
            }
        }

        private Rigidbody2D rb2D;

        private void Awake()
        {
            rb2D = GetComponent<Rigidbody2D>();
        }

        private void OnDrawGizmosSelected()
        {
            Gizmos.color = Color.blue;
            Gizmos.DrawLine(OriginPosition, OriginPosition + Vector3.down * maxDistanceFloorToJump);

            var hit = Physics2D.Raycast(OriginPosition, Vector3.down, maxDistanceFloorToJump, layerTrigger);
            Gizmos.color = Color.red;
            if (hit.collider != null)
                Gizmos.DrawLine(OriginPosition, hit.point);
        }

        public void Jump()
        {
            if (IsOnGround)
                rb2D.AddForce(Vector2.up * force, ForceMode2D.Impulse);
        }
    }
}